package com.cidenet.employees

import com.cidenet.employees.entity.EmployeeEntity
import com.cidenet.employees.repository.EmployeeRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc

import static org.assertj.core.api.Assertions.assertThat
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class EmployeesApplicationTests {
    private ObjectMapper objectMapper = new ObjectMapper()

    @Autowired
    private EmployeeRepository employeeRepository

    @Autowired
    private MockMvc mockMvc

    @BeforeEach
    void beforeEach() {
        employeeRepository.deleteAll()
    }

    EmployeeEntity createEmployee() {
        EmployeeEntity employeeEntity = new EmployeeEntity()

        employeeEntity.setFirstName("MILTON")
        employeeEntity.setMiddleName("LOPEZ")
        employeeEntity.setLastName("MONTERO")
        employeeEntity.setArea("RECURSOS HUMANOS")
        employeeEntity.setIdentificationType("PASAPORTE")
        employeeEntity.setIdentificationNumber("AV358319")
        employeeEntity.setEmploymentCountry("COLOMBIA")
        employeeEntity.setEntryDate(new Date())
        employeeEntity.setStatus("ACTIVO")
        employeeEntity.setCreation(new Date())

        return this.employeeRepository.save(employeeEntity)
    }

    @Test
    void getEmployeesReturn200() {
        this.mockMvc.perform(get("/v1/employee")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath('$.totalCount', is(0)))
                .andExpect(jsonPath('$.records.length()', is(0)))

        this.createEmployee()
        this.mockMvc.perform(get("/v1/employee")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath('$.totalCount', is(1)))
                .andExpect(jsonPath('$.records.length()', is(1)))
    }

    @Test()
    void newEmployeeReturn200() {
        Map data = ["firstName"           : "DAVID",
                    "middleName"          : "ANDRES",
                    "lastName"            : "AGUDELO",
                    "employmentCountry"   : "COLOMBIA",
                    "identificationType"  : "CEDULA DE CIUDADANIA",
                    "identificationNumber": "125",
                    "entryDate"           : "01/09/2022 10:30:00",
                    "area"                : "COMPRAS",]
        String dataJson = this.objectMapper.writeValueAsString(data)

        this.mockMvc.perform(post("/v1/employee").content(dataJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath('$.id', isA(Integer.class)))
                .andExpect(jsonPath('$.firstName', is("DAVID")))
                .andExpect(jsonPath('$.employmentCountry', is("COLOMBIA")))

        assertThat(employeeRepository.count(), 1)

        Iterable<EmployeeEntity> employeeEntities = this.employeeRepository.findAll()
        EmployeeEntity employeeEntity = employeeEntities.first()

        assertThat(employeeEntity.getFirstName(), "DAVID")
        assertThat(employeeEntity.getMiddleName(), "ANDRES")
        assertThat(employeeEntity.getLastName(), "AGUDELO")
        assertThat(employeeEntity.getIdentificationNumber(), "125")
        assertThat(employeeEntity.getArea(), "COMPRAS")
        assertThat(employeeEntity.getStatus(), "ACTIVO")
    }

    @Test()
    void newEmployeeReturn400() {
        Map data = ["firstName"           : "DAVID",
                    "middleName"          : "ANDRES",
                    "lastName"            : "AGUDELO",
                    "employmentCountry"   : "COLOMBIA",
                    "identificationType"  : "CEDULA DE CIUDADANIA",
                    "identificationNumber": "125",
                    "entryDate"           : "01/09/2022 10:30:00",
                    "area"                : "COMPRAS",
                    "status"              : "DESACTIVADO",
        ]
        String dataJson = this.objectMapper.writeValueAsString(data)

        this.mockMvc.perform(post("/v1/employee").content(dataJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath('$.errors.length()', is(1)))
    }

    @Test()
    void updateEmployeeReturn200() {
        EmployeeEntity employeeEntity = this.createEmployee()
        Map data = ["firstName"           : "JUANA",
                    "middleName"          : "GONZALEZ",
                    "lastName"            : "AGUDELO",
                    "employmentCountry"   : "COLOMBIA",
                    "identificationType"  : "CEDULA DE CIUDADANIA",
                    "identificationNumber": "512512",
                    "entryDate"           : "01/09/2022 10:30:00",
                    "area"                : "COMPRAS",]
        String dataJson = this.objectMapper.writeValueAsString(data)

        this.mockMvc.perform(put("/v1/employee/" + employeeEntity.getId()).content(dataJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath('$.id', isA(Integer.class)))
                .andExpect(jsonPath('$.firstName', is("JUANA")))
                .andExpect(jsonPath('$.middleName', is("GONZALEZ")))
                .andExpect(jsonPath('$.employmentCountry', is("COLOMBIA")))

        EmployeeEntity modifiedEmployeeEntity = this.employeeRepository.findById(employeeEntity.getId()).get()

        assertThat(modifiedEmployeeEntity.getFirstName(), "JUANA")
        assertThat(modifiedEmployeeEntity.getMiddleName(), "GONZALES")
        assertThat(modifiedEmployeeEntity.getLastName(), "AGUDELO")
        assertThat(modifiedEmployeeEntity.getIdentificationNumber(), "512512")
    }

    @Test()
    void updateEmployeeReturn404() {
        Map data = ["firstName"           : "JUANA",
                    "middleName"          : "GONZALEZ",
                    "lastName"            : "AGUDELO",
                    "employmentCountry"   : "COLOMBIA",
                    "identificationType"  : "CEDULA DE CIUDADANIA",
                    "identificationNumber": "512512",
                    "entryDate"           : "01/09/2022 10:30:00",
                    "area"                : "COMPRAS",]
        String dataJson = this.objectMapper.writeValueAsString(data)

        this.mockMvc.perform(put("/v1/employee/1000").content(dataJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
    }

    @Test()
    void deleteEmployeeReturn200() {
        EmployeeEntity employeeEntity = this.createEmployee()

        assertThat(this.employeeRepository.count(), 1)

        this.mockMvc.perform(delete("/v1/employee/" + employeeEntity.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

        assertThat(this.employeeRepository.count(), 0)
    }

    @Test()
    void deleteEmployeeReturn404() {
        this.mockMvc.perform(delete("/v1/employee/1000").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
    }
}
