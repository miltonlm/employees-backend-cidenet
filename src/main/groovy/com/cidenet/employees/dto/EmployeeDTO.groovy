package com.cidenet.employees.dto

import com.cidenet.employees.validation.In
import javax.validation.constraints.*

class EmployeeDTO {
    @Positive
    Long id
    @NotNull(message = "Can not be null.")
    @NotBlank
    @Size(max = 20)
    @Pattern(regexp = "[A-Z ]+", message = "Only upper case letters and no special characters.")
    String firstName
    @NotNull(message = "Can not be null.")
    @NotBlank
    @Size(max = 20)
    @Pattern(regexp = "[A-Z ]+", message = "Only upper case letters and no special characters.")
    String middleName
    @NotNull(message = "Can not be null.")
    @NotBlank
    @Size(max = 20)
    @Pattern(regexp = "[A-Z ]+", message = "Only upper case letters and no special characters.")
    String lastName
    @Size(max = 20)
    @Pattern(regexp = "[A-Z ]+", message = "Only upper case letters and no special characters.")
    String otherNames
    @NotNull(message = "Can not be null.")
    @In(value = ["COLOMBIA", "ESTADOS UNIDOS"], message = "Hello world")
    String employmentCountry
    @NotNull(message = "Must not be null.")
    @In(value = ["CEDULA DE CIUDADANIA", "CEDULA DE EXTRANJERIA", "PASAPORTE", "PERMISO ESPECIAL"],
            message = "Invalid value, must be one of CEDULA DE CIUDADANIA, CEDULA DE EXTRANJERIA, PASAPORTE, PERMISO ESPECIAL")
    String identificationType
    @NotNull(message = "Can not be null.")
    @NotBlank(message = "Must not be blank.")
    @Size(max = 20, message = "Max number of characters allowed is 20")
    @Pattern(regexp = "[A-Za-z0-9-]+", message = "Only the following characters are allowed: A-Z a-z 0-9 or -")
    String identificationNumber
    String email
    @NotNull(message = "Can not be null.")
    @Pattern(regexp = "[0-9]{2}/[0-9]{2}/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}", message = "Must a valid value in the format: DD/MM/YYYY HH:mm:ss")
    String entryDate
    @NotNull(message = "Can not be null.")
    @In(
            value = ["ADMINISTRACION", "FINANCIERA", "COMPRAS", "INFRAESTRUTURA", "OPERACION", "TALENTO HUMANO", "SERVICIOS VARIOS"],
            message = "Must be one of ADMINISTRACION, FINANCIERA, COMPRAS, INFRAESTRUTURA, OPERACION, TALENTO HUMANO, SERVICIOS VARIOS"
    )
    String area
    @In(value = ["ACTIVO"], message = "Must be either null or ACTIVO")
    String status
    String creation
}
