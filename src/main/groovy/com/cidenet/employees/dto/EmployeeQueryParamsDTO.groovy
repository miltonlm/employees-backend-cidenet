package com.cidenet.employees.dto

class EmployeeQueryParamsDTO {
    String firstName
    String middleName
    String lastName
    String otherNames
    String identificationType
    String identificationNumber
    String employmentCountry
    String email
    String status

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getMiddleName() {
        return middleName
    }

    void setMiddleName(String middleName) {
        this.middleName = middleName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getOtherNames() {
        return otherNames
    }

    void setOtherNames(String otherNames) {
        this.otherNames = otherNames
    }

    String getIdentificationType() {
        return identificationType
    }

    void setIdentificationType(String identificationType) {
        this.identificationType = identificationType
    }

    String getIdentificationNumber() {
        return identificationNumber
    }

    void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber
    }

    String getEmploymentCountry() {
        return employmentCountry
    }

    void setEmploymentCountry(String employmentCountry) {
        this.employmentCountry = employmentCountry
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }
}
