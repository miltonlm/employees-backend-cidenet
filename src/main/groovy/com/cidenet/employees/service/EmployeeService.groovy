package com.cidenet.employees.service

import com.cidenet.employees.dto.EmployeeDTO
import com.cidenet.employees.dto.EmployeeQueryParamsDTO
import com.cidenet.employees.entity.EmployeeEntity
import com.cidenet.employees.repository.EmployeeRepository
import org.modelmapper.ModelMapper
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

import javax.persistence.EntityNotFoundException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit

@Service
class EmployeeService {
    private final ModelMapper modelMapper = new ModelMapper()
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
    private final EmployeeRepository employeeRepository

    EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository
    }

    Map get(EmployeeQueryParamsDTO employeeQueryParamsDTO, Pageable pageable) {
        List<EmployeeDTO> employeeDTOList = []
        Map data = this.employeeRepository.findByEmployeeQueryParams(employeeQueryParamsDTO, pageable)

        data["records"].forEach {
            EmployeeDTO employeeDTO = this.modelMapper.map(it, EmployeeDTO.class)

            employeeDTO.metaClass = EmployeeDTO.metaClass
            employeeDTO.setEntryDate(this.simpleDateFormat.format(it.getEntryDate()))
            employeeDTO.setCreation(this.simpleDateFormat.format(it.getCreation()))

            employeeDTOList.add(employeeDTO)
        }
        data["records"] = employeeDTOList

        return data
    }

    EmployeeDTO create(EmployeeDTO employeeDTO) {
        validateEmployee(employeeDTO)

        String email = getNewEmail(employeeDTO)
        String entryDate = employeeDTO.getEntryDate()

        employeeDTO.setEntryDate(null)

        ModelMapper modelMapper = new ModelMapper()
        EmployeeEntity employeeEntity = modelMapper.map(employeeDTO, EmployeeEntity.class)

        employeeEntity.metaClass = EmployeeEntity.metaClass
        employeeEntity.setEmail(email)
        employeeEntity.setEntryDate(this.simpleDateFormat.parse(entryDate))
        employeeEntity.setStatus("ACTIVO")
        employeeEntity.setCreation(new Date())

        EmployeeEntity newEmployeeEntity = this.employeeRepository.save(employeeEntity)
        EmployeeDTO newEmployeeDTO = modelMapper.map(newEmployeeEntity, EmployeeDTO.class)

        newEmployeeDTO.metaClass = EmployeeDTO.metaClass
        newEmployeeDTO.setEntryDate(entryDate)
        newEmployeeDTO.setCreation(this.simpleDateFormat.format(newEmployeeEntity.getCreation()))

        return newEmployeeDTO
    }

    EmployeeDTO update(Long id, EmployeeDTO employeeDTO) {
        validateEmployee(employeeDTO, false, id)

        Optional<EmployeeEntity> existingEmployeeEntityOptional = this.employeeRepository.findById(id)
        EmployeeEntity existingEmployeeEntity = existingEmployeeEntityOptional.get()

        String email = existingEmployeeEntity.getEmail()

        if (existingEmployeeEntity.getFirstName() != employeeDTO.getFirstName() || existingEmployeeEntity.getMiddleName() != employeeDTO.getMiddleName()) {
            email = getNewEmail(employeeDTO)
        }

        String entryDate = employeeDTO.getEntryDate()

        employeeDTO.setEntryDate(null)

        existingEmployeeEntity.setId(existingEmployeeEntity.getId())
        existingEmployeeEntity.setFirstName(employeeDTO.getFirstName())
        existingEmployeeEntity.setMiddleName(employeeDTO.getMiddleName())
        existingEmployeeEntity.setLastName(employeeDTO.getLastName())
        existingEmployeeEntity.setOtherNames(employeeDTO.getOtherNames())
        existingEmployeeEntity.setIdentificationType(employeeDTO.getIdentificationType())
        existingEmployeeEntity.setIdentificationNumber(employeeDTO.getIdentificationNumber())
        existingEmployeeEntity.setEmploymentCountry(employeeDTO.getEmploymentCountry())
        existingEmployeeEntity.setEmail(email)
        existingEmployeeEntity.setArea(employeeDTO.getArea())
        existingEmployeeEntity.setEntryDate(this.simpleDateFormat.parse(entryDate))
        existingEmployeeEntity.setStatus(employeeDTO.getStatus())

        EmployeeEntity modifiedEmployeeEntity = this.employeeRepository.save(existingEmployeeEntity)
        EmployeeDTO modifiedEmployeeDTO = this.modelMapper.map(modifiedEmployeeEntity, EmployeeDTO.class)

        modifiedEmployeeDTO.metaClass = EmployeeDTO.metaClass
        modifiedEmployeeDTO.setEntryDate(entryDate)
        modifiedEmployeeDTO.setCreation(this.simpleDateFormat.format(existingEmployeeEntity.getCreation()))

        return modifiedEmployeeDTO
    }

    void validateEmployee(EmployeeDTO employeeDTO, boolean isNew = true, Long id = null) {
        Optional<EmployeeEntity> existingEmployee = this.employeeRepository.findByIdentificationNumberAndIdentificationType(employeeDTO.getIdentificationNumber(), employeeDTO.getIdentificationType())

        if (existingEmployee.isPresent() && isNew) {
            throw new IllegalStateException("An employee with this identification number and identification type already exists.")
        }

        if (!isNew) {
            Optional<EmployeeEntity> existingEmployeeEntity = this.employeeRepository.findById(id)

            if (!existingEmployeeEntity.isPresent()) {
                throw new EntityNotFoundException(String.format("No employee found with id %d", id))
            }
        }

        Date entryDate = this.simpleDateFormat.parse(employeeDTO.getEntryDate())
        LocalDateTime entryLocalDateTime = entryDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
        LocalDateTime entryMin = LocalDateTime.now().minus(1, ChronoUnit.MONTHS)

        if (entryLocalDateTime.isAfter(LocalDateTime.now())) {
            throw new IllegalStateException("The entryDate can not be future.")
        }

        if (entryLocalDateTime.isBefore(entryMin)) {
            throw new IllegalStateException("The entryDate can not be less than a month before.")
        }
    }

    String getNewEmail(EmployeeDTO employeeDTO) {
        String emailUserName = employeeDTO.getFirstName().toLowerCase().replace(" ", "") + "." + employeeDTO.getMiddleName().toLowerCase().replace(" ", "")
        String emailHost = (employeeDTO.getEmploymentCountry() == "COLOMBIA") ? "cidenet.com.co" : "cidenet.com.us"
        String email = emailUserName + "@" + emailHost
        int emailCount = 0

        while (this.employeeRepository.findByEmail(email).size() > 0) {
            emailCount++
            email = emailUserName + "." + emailCount + "@" + emailHost
        }

        return email
    }

    EmployeeDTO delete(Long id) {
        Optional<EmployeeEntity> employeeEntityOptional = this.employeeRepository.findById(id)

        if (!employeeEntityOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("No employee found with id %d", id))
        }

        EmployeeEntity employeeEntity = employeeEntityOptional.get()

        this.employeeRepository.delete(employeeEntity)

        EmployeeDTO deletedEmployee = this.modelMapper.map(employeeEntityOptional.get(), EmployeeDTO.class)

        deletedEmployee.metaClass = EmployeeDTO.metaClass
        deletedEmployee.setEntryDate(this.simpleDateFormat.format(employeeEntity.getEntryDate()))
        deletedEmployee.setCreation(this.simpleDateFormat.format(employeeEntity.getCreation()))

        return deletedEmployee
    }
}
