package com.cidenet.employees.validation

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class InValidator implements ConstraintValidator<In, String> {
    String[] value;

    @Override
    void initialize(In constraintAnnotation) {
        this.value = constraintAnnotation.value()
    }

    @Override
    boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return true
        }
        return s in value;
    }
}
