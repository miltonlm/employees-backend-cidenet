package com.cidenet.employees.validation

import javax.validation.Constraint
import javax.validation.Payload
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.Target
import static java.lang.annotation.RetentionPolicy.*;
import static java.lang.annotation.ElementType.*;

@Target([ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE ])
@Retention(RUNTIME)
@Constraint(validatedBy = InValidator.class)
@Documented
@interface In {
    Class<?>[] groups() default [];
    Class<? extends Payload>[] payload() default [];
    String message()
    String[] value()
}
