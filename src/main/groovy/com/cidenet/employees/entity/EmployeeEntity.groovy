package com.cidenet.employees.entity

import groovy.transform.Canonical

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "employee")
class EmployeeEntity {
    @Id
    @GeneratedValue()
    Long id
    String firstName
    String middleName
    String lastName
    String otherNames
    String employmentCountry
    String identificationType
    String identificationNumber
    String email
    Date entryDate
    String area
    String status
    Date creation

    Long getId() {
        return id
    }

    void setId(Long id) {
        this.id = id
    }

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getMiddleName() {
        return middleName
    }

    void setMiddleName(String middleName) {
        this.middleName = middleName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getOtherNames() {
        return otherNames
    }

    void setOtherNames(String otherNames) {
        this.otherNames = otherNames
    }

    String getEmploymentCountry() {
        return employmentCountry
    }

    void setEmploymentCountry(String employmentCountry) {
        this.employmentCountry = employmentCountry
    }

    String getIdentificationType() {
        return identificationType
    }

    void setIdentificationType(String identificationType) {
        this.identificationType = identificationType
    }

    String getIdentificationNumber() {
        return identificationNumber
    }

    void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    Date getEntryDate() {
        return entryDate
    }

    void setEntryDate(Date entryDate) {
        this.entryDate = entryDate
    }

    String getArea() {
        return area
    }

    void setArea(String area) {
        this.area = area
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    Date getCreation() {
        return creation
    }

    void setCreation(Date creation) {
        this.creation = creation
    }
}
