package com.cidenet.employees.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

import javax.persistence.EntityNotFoundException

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ControllerAdvice
class GlobalExceptionHandler {
    Logger logger = LogManager.getLogger(GlobalExceptionHandler.class)

    @ExceptionHandler(value = [MethodArgumentNotValidException.class])
    ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        List<Map> errors = []

        exception.getFieldErrors().forEach {
            errors.add(Map.of(it.getField(), it.getDefaultMessage()))
        }

        logger.info("Returning a 400 bad request: {}", errors)

        return ResponseEntity.badRequest().body(Map.of("errors", errors))
    }

    @ExceptionHandler(value = [IllegalStateException.class])
    ResponseEntity<Object> illegalStateException(IllegalStateException exception) {
        logger.info("Returning a 400 bad request: {}", exception.getMessage())
        return ResponseEntity.badRequest().body(Map.of("error", exception.getMessage()))
    }

    @ExceptionHandler(value = [EntityNotFoundException.class])
    ResponseEntity<Object> entityNotFoundException(EntityNotFoundException exception) {
        logger.info("Returning a 404 request: {}", exception.getMessage())
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", exception.getMessage()))
    }
}
