package com.cidenet.employees.repository

import com.cidenet.employees.dto.EmployeeQueryParamsDTO
import org.springframework.data.domain.Pageable

interface EmployeeCustomRepository {
    Map findByEmployeeQueryParams(EmployeeQueryParamsDTO employeeQueryParamsDTO, Pageable pageable)
}