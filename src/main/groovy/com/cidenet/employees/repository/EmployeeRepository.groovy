package com.cidenet.employees.repository

import com.cidenet.employees.entity.EmployeeEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long>, EmployeeCustomRepository {
    Optional<EmployeeEntity> findByIdentificationNumberAndIdentificationType(String identification, String identificationType)
    List<EmployeeEntity> findByEmail(String email)
}
