package com.cidenet.employees.repository

import com.cidenet.employees.dto.EmployeeQueryParamsDTO
import com.cidenet.employees.entity.EmployeeEntity
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

import javax.persistence.EntityManager
import javax.persistence.TypedQuery
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

@Component
class EmployeeCustomRepositoryImpl implements EmployeeCustomRepository {
    private EntityManager entityManager

    EmployeeCustomRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager
    }

    @Override
    Map findByEmployeeQueryParams(EmployeeQueryParamsDTO employeeQueryParamsDTO, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder()
        CriteriaQuery<EmployeeEntity> criteriaQuery = criteriaBuilder.createQuery(EmployeeEntity.class)
        Root<EmployeeEntity> root = criteriaQuery.from(EmployeeEntity.class)
        List<Predicate> predicateList = []

        if (employeeQueryParamsDTO.getFirstName() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("firstName"), employeeQueryParamsDTO.getFirstName()))
        }

        if (employeeQueryParamsDTO.getMiddleName() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("middleName"), employeeQueryParamsDTO.getMiddleName()))
        }

        if (employeeQueryParamsDTO.getLastName() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("lastName"), employeeQueryParamsDTO.getLastName()))
        }

        if (employeeQueryParamsDTO.getOtherNames() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("otherNames"), employeeQueryParamsDTO.getOtherNames()))
        }

        if (employeeQueryParamsDTO.getIdentificationType() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("identificationType"), employeeQueryParamsDTO.getIdentificationType()))
        }

        if (employeeQueryParamsDTO.getIdentificationNumber() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("identificationNumber"), employeeQueryParamsDTO.getIdentificationNumber()))
        }

        if (employeeQueryParamsDTO.getEmploymentCountry() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("employmentCountry"), employeeQueryParamsDTO.getEmploymentCountry()))
        }

        if (employeeQueryParamsDTO.getEmail() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("email"), employeeQueryParamsDTO.getEmail()))
        }

        if (employeeQueryParamsDTO.getStatus() != null) {
            predicateList.add(criteriaBuilder.equal(root.get("status"), employeeQueryParamsDTO.getStatus()))
        }

        criteriaQuery.where((Predicate[]) predicateList.toArray(new Predicate[0]))

        TypedQuery<EmployeeEntity> query = entityManager.createQuery(criteriaQuery)
        int count = query.getResultList().size()
        int pageNumber = pageable.pageNumber

        if (pageNumber == 0) {
            pageNumber = 1
        }

        query.setMaxResults(pageable.pageSize)
        query.setFirstResult((pageNumber - 1) * pageable.pageSize)

        return [
                "totalCount": count,
                "pages": Math.ceil(count / pageable.pageSize).intValue(),
                "currentPage": pageNumber,
                "records": query.getResultList(),
        ]
    }
}
