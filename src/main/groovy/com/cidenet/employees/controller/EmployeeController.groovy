package com.cidenet.employees.controller

import com.cidenet.employees.dto.EmployeeDTO
import com.cidenet.employees.dto.EmployeeQueryParamsDTO
import com.cidenet.employees.service.EmployeeService
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@RequestMapping(path = "/v1/employee")
class EmployeeController {
    private final EmployeeService employeeService
    private final Logger logger = LogManager.getLogger(EmployeeController.class)

    @Autowired
    EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService
    }

    @GetMapping
    Map get(EmployeeQueryParamsDTO employeeQueryParamsDTO,
                          @PageableDefault(size = 10, page = 1) Pageable pageable) {
        logger.info("Received a GET request on /employee with parameters: {}", new ObjectMapper().writeValueAsString(employeeQueryParamsDTO))
        return this.employeeService.get(employeeQueryParamsDTO, pageable)
    }

    @PostMapping
    EmployeeDTO create(@RequestBody @Valid EmployeeDTO employeeDTO) {
        logger.info("Received a POST request on /employee with parameters: {}", new ObjectMapper().writeValueAsString(employeeDTO))
        return this.employeeService.create(employeeDTO)
    }

    @PutMapping("/{id}")
    EmployeeDTO update(@PathVariable("id") Integer id, @RequestBody @Valid EmployeeDTO employeeDTO) {
        logger.info("Received a PUT request on /employee with parameters: {}, {}", id, new ObjectMapper().writeValueAsString(employeeDTO))
        return this.employeeService.update(id, employeeDTO)
    }

    @DeleteMapping("/{id}")
    EmployeeDTO delete(@PathVariable("id") Integer id) {
        logger.info("Received a DELETE request on /employee with parameters: {}", id)
        return this.employeeService.delete(id)
    }

}
