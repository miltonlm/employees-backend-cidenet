## Employees backend API

This is an API backend that allows users to perform CRUD operations on an employees table.
The software is made using:

- Java 11.
- Spring Boot.
- Groovy.
- H2 In-memory database.
- Spring Data.
- Hibernate Validation API.

## Running the services

To run the services just need to run the command:

- ./mvnw spring-boot:run

Or to run the tests:

- ./mvnw test

No other configuration is need to run or test the project.

## Available endpoints

The following are the available endpoints:

- GET /v1/employee
- POST /v1/employee
- PUT /v1/employee/{id}
- DELETE /v1/employee/{id}
